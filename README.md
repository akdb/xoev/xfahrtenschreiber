# Standard XFahrtenschreiber

**XFahrtenschreiber** ist ein [XÖV](https://www.xoev.de/xoev-4987
"XÖV")-Standard für die elektronische Kommunikation mit den Ausgabestellen von
Fahrtenschreiberkarten.

Die XML Schema-Definitionen eines XÖV-Standards basieren auf der von dem W3C
empfohlenen XML Schema-Definitionssprache.  Alle im Rahmen eines XÖV-Standards
definierten globalen XML-Elemente und benannten XML-Typen müssen sich in einem
Namensraum befinden, der den betroffenen XÖV-Standard eindeutig identifiziert.
Jede XML Schema-Definition eines XÖV-Standards muss versioniert sein.

Das XÖV-Handbuch empfiehlt, den physischen Speicherort einer XML
Schema-Definition in Form einer öffentlichen URL anzugeben. Die hier
vorliegende versionierte Ablage der **XFahrtenschreiber** Schema-Definitionen
dient genau dem Zweck, einen öffentlicher Zugang zu den XML Schema-Definitionen
des XÖV-Standards [XFahrtenschreiber](https://www.xrepository.de/details/urn:xoev-de:hsm:standard:xfahrtenschreiber)
bereitzustellen und XML Schema-Validatoren
einen direkten Zugriff zu ermöglichen.

## Releases

Releases des Standards **XFahrtenschreiber** sind im
[XRepository](https://www.xrepository.de/details/urn:xoev-de:hsm:standard:xfahrtenschreiber)
abgelegt. Der Herausgeber ist das [Hessisches Ministerium für Soziales und
Integration](https://soziales.hessen.de/ "HSM")

Die Releases des Standards **XFahrtenschreiber** umfassen folgende Bestandteile:

- **UML-Fachmodell (XMI)**: Das UML-Modell enthält die Datenstrukturen mit den
  entsprechenden semantischen Definitionen.

- **XML-Schema-Definitionen (XSD):** Die XML-Schemadateien definieren das
  XML-Format der **XFahrtenschreiber** Antragsdaten.

- **Spezifikation (PDF):** Die **XFahrtenschreiber** Spezifikation beinhaltet unter
  anderem das Datenstrukturmodell, die Beschreibung der Nachrichten, das
  Informationsmodell  und die Codelisten von **XFahrtenschreiber**.  Die
  **XFahrtenschreiber** Spezifikation stellt im Anhang F die Versionshistorie
  zur Verfügung. Die Unterkapitel von Anhang F beinhalten die Release Notes für
  alle bisherigen **XFahrtenschreiber** Versionen. Dort werden alle Änderungen
  zur jeweiligen **XFahrtenschreiber** Vorgängerversion aufgelistet. 

Version: 0.1.7

## Lizenzen

Die XÖV Standards sind, entsprechend dem XÖV Handbuch, offene und
lizenzkostenfreie Standards, die allen Interessierten frei zugänglich zur
Verfügung stehen, somit Open-Source.

## Kontakt

Mailkontakt: OZG@hsm.hessen.de
