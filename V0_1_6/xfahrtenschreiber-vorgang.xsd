<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xfahrtenschreiber="https://gitlab.opencode.de/akdb/xoev/xfahrtenschreiber/-/raw/main/V0_1_6"
           xmlns:din91379="urn:xoev-de:kosit:xoev:datentyp:din-91379_2022-08"
           xmlns:dinspec91379="urn:xoev-de:kosit:xoev:datentyp:din-spec-91379_2019-03"
           targetNamespace="https://gitlab.opencode.de/akdb/xoev/xfahrtenschreiber/-/raw/main/V0_1_6"
           version="0.1.6"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XFahrtenschreiber</nameLang>
            <nameKurz>XFahrtenschreiber</nameKurz>
            <nameTechnisch>xfahrtenschreiber</nameTechnisch>
            <kennung>urn:xoev-de:hsm:standard:xfahrtenschreiber</kennung>
            <beschreibung>XFahrtenschreiber - XÖV-Standard für die elektronische Kommunikation mit den Ausgabestellen von Fahrtenschreiberkarten.</beschreibung>
         </standard>
         <versionStandard>
            <version>0.1.6</version>
            <versionXOEVProfil>3.0.1</versionXOEVProfil>
            <versionXOEVHandbuch>3.0</versionXOEVHandbuch>
            <versionXGenerator>3.1.1</versionXGenerator>
            <versionModellierungswerkzeug>2022x</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
   </xs:annotation>
   <xs:include schemaLocation="xfahrtenschreiber-baukasten.xsd"/>
   <xs:import schemaLocation="http://xoev.de/DIN-SPEC-91379/2019-03/datatypes/din-91379-datatypes.xsd"
              namespace="urn:xoev-de:kosit:xoev:datentyp:din-spec-91379_2019-03"/>
   <xs:import schemaLocation="https://xoev.de/schemata/din/91379/2022-08/din-norm-91379-datatypes.xsd"
              namespace="urn:xoev-de:kosit:xoev:datentyp:din-91379_2022-08"/>
   <xs:element name="vorgang.transportieren.2010">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>FPersV § 2</rechtsgrundlage>
            <title>Vorgang transportieren</title>
         </xs:appinfo>
         <xs:documentation>Transport eines Vorgangs (Antrag/Fall/Bescheid) zwischen den Partnern. Dies ist immer die erste Nachricht für die Abwicklung eines kompletten, fallbezogenen Antragsverfahren. Diese Nachricht wird zum Beispiel zur Übermittlung des Online-Antrags für folgenden Leistungskombinationen genutzt: Erteilung einer Fahrerkarte Erneuerung einer Fahrerkarte Ersatz einer Fahrerkarte Erstausstellung einer Werkstattkarte Erteilung einer Werkstattkarte Ersatz einer Werkstattkarte Erteilung einer oder mehrerer Unternehmenskarten Erneuerung einer Unternehmenskarte Ersatz einer Unternehmenskarte</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xfahrtenschreiber:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="antragsnummer" minOccurs="0" type="din91379:datatypeC">
                     <xs:annotation>
                        <xs:documentation>Eindeutige Nummer des Antrags Diese Nummer ist nicht zu verwechseln mit der nachrichtenUUID oder der vorgangsID.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="vorgang" type="xfahrtenschreiber:Vorgang">
                     <xs:annotation>
                        <xs:documentation>Hier werden die Daten zum Antrag/Vorgang übermittelt.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="vorgang.nachricht.2020">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>FPersV § 2</rechtsgrundlage>
            <title>Freie Nachricht transportieren</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht dient zum Transport von Informationen in Freitext, zum Beispiel anstatt einer E-Mail oder eines Telefonanrufes, oder zur Übermittlung von Unterlagen zu einem bereits existierenden Vorgang. Dieser Nachricht muss zuerst eine Nachricht vom Typ vorgang.transportieren.2010 vorausgegangen sein. Der Vorgang, auf den sich diese Nachricht bezieht, wird mit dem Element identifikationVorgang referenziert. Die Nachricht, auf die Bezug genommen wird, ist im Feld nachrichtenkopf/referenzUUID anzugeben</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xfahrtenschreiber:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="identifikationVorgang"
                              type="xfahrtenschreiber:Identifikation.Vorgang">
                     <xs:annotation>
                        <xs:documentation>Eindeutige Identifizierung des Vorgangs, auf den Bezug genommen werden soll</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="freieNachricht"
                              minOccurs="0"
                              type="xfahrtenschreiber:FreieNachricht">
                     <xs:annotation>
                        <xs:documentation>Hier werden Freitextinformation übertragen.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlage"
                              minOccurs="0"
                              maxOccurs="unbounded"
                              type="xfahrtenschreiber:Dokument">
                     <xs:annotation>
                        <xs:documentation>Zum Vorgang oder zur freien Nachricht gehörige Unterlage(n)</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="vorgang.statusabfrage.2030">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>FPersV § 2</rechtsgrundlage>
            <title>Status abfragen</title>
         </xs:appinfo>
         <xs:documentation>Mit dieser Nachricht wird der aktuelle fachliche Status eines Vorgangs explizit angefragt. Der Empfänger der Nachricht antwortet auf diese Nachricht mit der Nachricht administration.quittung.0020 und gibt dann im Element quittung-&gt;aktuellerStatusFachlich den aktuellen fachlichen Status zurück.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xfahrtenschreiber:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="identifikationVorgang"
                              type="xfahrtenschreiber:Identifikation.Vorgang">
                     <xs:annotation>
                        <xs:documentation>Eindeutige Identifizierung des Vorgangs, zu dem der Status abgefragt werden soll.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
</xs:schema>
